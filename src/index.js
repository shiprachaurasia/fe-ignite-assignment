import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import Card from './components/cardComponent';

ReactDOM.render(<Card />, document.getElementById('root'));
registerServiceWorker();
