import React, { Component } from "react";
import './cardComponent.css';
class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      numberOfStarList: [],
      objectDetails: [],
      items: []
    };
  }

  render() {
    return (
      <div className="container">
        {this.state.objectDetails.map(
          (item, key)  => {
            return (
                <div className="col-md-3 card" key={key}>
                  <img
                    className="card-img-top" title = {item.full_name}
                    src={item.loggedInAvatar_url}
                    alt="not found"
                  />
                  <div className = "card-body">
                    <a
                      className="card-title"
                      href={item.html_url}
                      target="_blank"
                    >
                      <span>{item.full_name}</span>
                    </a>
                    <br />
                    <a
                      className="card-text"
                      href={item.loggedInHtmlUrl}
                      target="_blank"
                    >
                      <span>{item.loggedInOwner}</span>
                    </a>
                    {this.state.numberOfStarList.map((star, index) => {
                      
                      return (
                        star.currentKEy[item.full_name] ? 
                         <div className = "starDetails" key = {index}>
                         <i className="fa fa-star"></i> &nbsp;&nbsp;
                      <span className="card-text" title = "Number Of Stars">{star.currentKEy[item.full_name]}</span> 
                    </div> : null
                    )
                    }
                    )}
                    </div>
                   
                </div>
            );
          }
        )}
      </div>
    );
  }
  componentDidMount() {
    fetch("https://api.github.com/repositories")
      .then(res => res.json())
      .then(result => {
        this.setState({
          isLoaded: true,
          items: result
        });
        for (var i = 0; i < 5; i++) {
          this.state.objectDetails.push({
            full_name: this.state.items[i].full_name,
            html_url: this.state.items[i].html_url,
            stargazers_url: this.state.items[i].stargazers_url,
            loggedInOwner: this.state.items[i].owner.login,
            loggedInHtmlUrl: this.state.items[i].owner.html_url,
            loggedInAvatar_url: this.state.items[i].owner.avatar_url
          });
           
          let  starIndetifierNAme = this.state.objectDetails[i].full_name;
          fetch(this.state.objectDetails[i].stargazers_url)
            .then(res => res.json())
            .then(result => {
              let items = result;      
              let numberOfStarListArr = this.state.numberOfStarList;
              numberOfStarListArr.push({
                currentKEy: { [starIndetifierNAme]: items.length } ,
                listData: items
              });
              this.setState({
                numberOfStarList: numberOfStarListArr,
                isLoaded: true,
                items: items
              })
            });
        }
      });
  }
}

export default Card;
